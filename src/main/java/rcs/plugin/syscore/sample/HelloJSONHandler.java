/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rcs.plugin.syscore.sample;

import id.co.vsi.common.ISO8583.MessageType;
import id.co.vsi.common.ISO8583.MessageVersion;
import id.co.vsi.common.JSON.JSONMessage;
import id.co.vsi.common.constants.ResponseCode;
import id.co.vsi.systemcore.jasoncore.JSONPlugin;
import id.co.vsi.systemcore.jasoncore.JSONPluginHandler;

/**
 * @author $Author:: ranis $: Author of last revision
 * @version $Revision:: 92 $: Last revision number
 * @since $LastChangedDate:: 2013-01-18 17:46:08 +0700 (Fri, 18 Jan 2013) $:
 * Date of last revision
 */
public class HelloJSONHandler extends JSONPluginHandler {

    public HelloJSONHandler(JSONPlugin pParentPlugin) {
        super(pParentPlugin);
    }

    @Override
    public boolean canHandleMessage(JSONMessage pDownlineReq) {
        return MessageType.ADMINISTRATIVE_REQUEST.getMessageTypeCode(MessageVersion.VERSION_2003)
                .equals(pDownlineReq.getString(Constant.cFieldMTI));
    }

    @Override
    public JSONMessage handleMessage(JSONMessage pDownlineReq) {
        JSONMessage tResp = new JSONMessage(pDownlineReq);
        tResp.setString("MTI","2610");
        tResp.setString(Constant.cFieldRC, ResponseCode.SUCCESS.getResponseCodeString());
        tResp.setString(Constant.cFieldRCM, "Hello");
        return tResp;
    }

    @Override
    public String getNameHandler() {
        return this.getClass().getSimpleName();
    }
}
